﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayRoutes.Api.Business.Models
{
    public class Register
    {
        public int Id { get; set; }
        public int OriginCityName { get; set; }
        public int DestinationCityName { get; set; }
    }
}
