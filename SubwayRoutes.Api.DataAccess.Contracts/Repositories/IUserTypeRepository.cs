﻿using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts.Repositories
{
    public interface IUserTypeRepository : IRepository<UserTypeEntity>
    {
        Task<UserTypeEntity> Update(UserTypeEntity entity);

    }
}
