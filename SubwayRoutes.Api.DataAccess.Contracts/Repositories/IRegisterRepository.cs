﻿using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts.Repositories
{
    public interface IRegisterRepository : IRepository<RegisterEntity>
    {
        Task<RegisterEntity> Update(RegisterEntity entity);

    }
}
