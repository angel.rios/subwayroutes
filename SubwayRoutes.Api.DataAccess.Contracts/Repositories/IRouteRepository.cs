﻿using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts.Repositories
{
    public interface IRouteRepository : IRepository<RouteEntity>
    {
        Task<RouteEntity> Update(RouteEntity entity);

    }
}
