﻿using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts.Repositories
{
    public interface ICityRepository : IRepository<CityEntity>
    {
        Task<CityEntity> Update(CityEntity entity);

    }
}
