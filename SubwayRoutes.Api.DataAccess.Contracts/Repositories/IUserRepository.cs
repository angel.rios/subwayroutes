﻿using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts.Repositories
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        Task<UserEntity> Update(int idEntity);
    }
}
