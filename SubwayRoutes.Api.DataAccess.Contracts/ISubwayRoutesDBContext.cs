﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Contracts
{
    public interface ISubwayRoutesDBContext
    {
        DbSet<UserEntity> Users { get; set; }
        DbSet<UserTypeEntity> UsersType { get; set; }
        DbSet<CityEntity> Citys { get; set; }
        DbSet<RouteEntity> Routes { get; set; }
        DbSet<RegisterEntity> Register { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DatabaseFacade Database { get; }
        Task<int> SaveChangeAsync(CancellationToken cancellationToken = default(CancellationToken));
        void RemoveRange(IEnumerable<object> entities);
        EntityEntry Update(object entity);

    }
}
