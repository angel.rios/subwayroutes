﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayRoutes.Api.DataAccess.Contracts.Entities
{
    public class UserTypeEntity
    {
        public int Id { get; set; }
        public string TypeName { get; set; }

        public virtual UserEntity User { get; set; }




    }
}
