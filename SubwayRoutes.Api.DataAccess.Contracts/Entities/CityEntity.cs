﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayRoutes.Api.DataAccess.Contracts.Entities
{
    public class CityEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }
}
