﻿
namespace SubwayRoutes.Api.DataAccess.Contracts.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string Cedula { get; set; }

        public string Password { get; set; }
        public int UserTypeId { get; set; }

        public virtual UserTypeEntity UserType { get; set; }
        public object Name { get; set; }
    }
}
