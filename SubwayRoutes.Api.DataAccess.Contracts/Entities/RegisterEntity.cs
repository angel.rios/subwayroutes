﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayRoutes.Api.DataAccess.Contracts.Entities
{
    public class RegisterEntity
    {
        public int Id { get; set; }
        public int OriginCityName { get; set; }
        public int DestinationCityName { get; set; }        


    }
}
