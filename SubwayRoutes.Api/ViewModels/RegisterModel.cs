﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.ViewModels
{
    public class RegisterModel
    {
        public int Id { get; set; }
        public int OriginCityName { get; set; }
        public int DestinationCityName { get; set; }

    }
}
