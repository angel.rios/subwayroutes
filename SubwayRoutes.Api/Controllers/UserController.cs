﻿using Microsoft.AspNetCore.Mvc;
using SubwayRoutes.Api.Services.Contracts.Services;
using System.Collections.Generic;

namespace SubwayRoutes.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {


        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }        

        
        [HttpGet]
        public ActionResult<IEnumerable<string>> GetById(int id)
        {
            return new string[] { "Hola" + id };
        }
    }
}
