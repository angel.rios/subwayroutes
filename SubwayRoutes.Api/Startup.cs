using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using SubwayRoutes.Api.DataAccess;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.Transversal.Register;

namespace SubwayRoutes.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddTransient<ISubwayRoutesDBContext, SubwayRoutesDBContext>();
            IoCRegister.AddRegistration(services);

            services.AddDbContext<SubwayRoutesDBContext>(options =>
                        options.UseNpgsql(Configuration.GetConnectionString("DataBaseConnection")));

            //services.AddDbContext<SubwayRoutesDBContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DataBaseConnection")));
            //services.AddApplicationInsightsTelemetry(Configuration);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Subway Routes Swagger", Version = "v1" });
            });

            //services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(config =>
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger SubwayRoutes"));

            //app.UseMvc();
        }
    }
}
