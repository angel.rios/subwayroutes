﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;

namespace SubwayRoutes.Api.DataAccess.EntityConfig
{
    public class UserTypeEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<UserTypeEntity> entityBuilder)
        {

            entityBuilder.ToTable("UsersType");
            entityBuilder.HasKey(x => x.Id);
            entityBuilder.Property(x => x.Id).IsRequired();
            


        }

    }
}
