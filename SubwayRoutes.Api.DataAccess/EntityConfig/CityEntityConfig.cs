﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;

namespace SubwayRoutes.Api.DataAccess.EntityConfig
{
    public class CityEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<CityEntity> entityBuilder)
        {

            entityBuilder.ToTable("Citys");
            entityBuilder.HasKey(x => x.Id);
            entityBuilder.Property(x => x.Id).IsRequired();

        }

    }
}
