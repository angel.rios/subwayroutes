﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;

namespace SubwayRoutes.Api.DataAccess.EntityConfig
{
    public class RouteEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<RouteEntity> entityBuilder)
        {

            entityBuilder.ToTable("Routes");
            entityBuilder.HasKey(x => x.Id);
            entityBuilder.Property(x => x.Id).IsRequired();

        }

    }
}
