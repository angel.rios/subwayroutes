﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;

namespace SubwayRoutes.Api.DataAccess.EntityConfig
{
    public class RegisterEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<RegisterEntity> entityBuilder)
        {

            entityBuilder.ToTable("Registers");
            entityBuilder.HasKey(x => x.Id);
            entityBuilder.Property(x => x.Id).IsRequired();

        }

    }
}
