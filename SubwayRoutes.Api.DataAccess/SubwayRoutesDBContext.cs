﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.EntityConfig;
using System.Threading;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess
{

    public class SubwayRoutesDBContext : DbContext, ISubwayRoutesDBContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<UserTypeEntity> UsersType { get; set; }
        public DbSet<CityEntity> Citys { get; set; }
        public DbSet<RouteEntity> Routes { get; set; }
        public DbSet<RegisterEntity> Register { get; set; }


        public SubwayRoutesDBContext(DbContextOptions options) : base(options) { }

        public SubwayRoutesDBContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=SubwayRoutesDB;Username=postgres;Password=123");
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserEntityConfig.SetEntityBuilder(modelBuilder.Entity<UserEntity>());
            UserTypeEntityConfig.SetEntityBuilder(modelBuilder.Entity<UserTypeEntity>());
            CityEntityConfig.SetEntityBuilder(modelBuilder.Entity<CityEntity>());
            RouteEntityConfig.SetEntityBuilder(modelBuilder.Entity<RouteEntity>());
            RegisterEntityConfig.SetEntityBuilder(modelBuilder.Entity<RegisterEntity>());


            base.OnModelCreating(modelBuilder);

        }

        public Task SaveChangesAsync()
        {
            throw new System.NotImplementedException();
        }

        new public DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            throw new System.NotImplementedException();
        }

        public Task<int> SaveChangeAsync(CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }
    }
}
