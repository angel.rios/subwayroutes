﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class RegisterRepository : IRegisterRepository
    {
        private ISubwayRoutesDBContext _subwayRoutesDBContext;

        public RegisterRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }
        public async Task<RegisterEntity> Add(RegisterEntity entity)
        {
            await _subwayRoutesDBContext.Register.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return entity;
        }
        public async Task<RegisterEntity> Update(RegisterEntity entity)
        {
            var upDateEntity = _subwayRoutesDBContext.Register.Update(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return upDateEntity.Entity;
        }
        public async Task<RegisterEntity> Get(int idEntity)
        {

            var result = await _subwayRoutesDBContext.Register.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }
        public async Task<RegisterEntity> Update(int idEntity, RegisterEntity upDateEntity)
        {
            var entity = await Get(idEntity);
            entity.Id = upDateEntity.Id;

            var result = _subwayRoutesDBContext.Update(entity);

            return entity;
        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<RegisterEntity>> GetAll()
        {
            throw new NotImplementedException();
        }



        public Task<RegisterEntity> Update(int idEntity)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.Register.SingleAsync(x => x.Id == id);
            _subwayRoutesDBContext.Register.Remove(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();
        }
    }
}
