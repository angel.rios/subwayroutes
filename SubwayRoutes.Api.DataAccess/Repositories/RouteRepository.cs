﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class RouteRepository : IRouteRepository
    {
        private ISubwayRoutesDBContext _subwayRoutesDBContext;

        public RouteRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }
        public async Task<RouteEntity> Add(RouteEntity entity)
        {
            await _subwayRoutesDBContext.Routes.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return entity;
        }
        public async Task<RouteEntity> Update(RouteEntity entity)
        {
            var upDateEntity = _subwayRoutesDBContext.Routes.Update(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return upDateEntity.Entity;
        }
        public async Task<RouteEntity> Get(int idEntity)
        {

            var result = await _subwayRoutesDBContext.Routes.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }
        public async Task<RouteEntity> Update(int idEntity, RouteEntity upDateEntity)
        {
            var entity = await Get(idEntity);
            entity.Name = upDateEntity.Name;

            var result = _subwayRoutesDBContext.Update(entity);

            return entity;
        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<RouteEntity>> GetAll()
        {
            throw new NotImplementedException();
        }



        public Task<RouteEntity> Update(int idEntity)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.Routes.SingleAsync(x => x.Id == id);
            _subwayRoutesDBContext.Routes.Remove(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();
        }
    }
}
