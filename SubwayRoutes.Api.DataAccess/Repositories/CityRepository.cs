﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class CityRepository : ICityRepository
    {
        private ISubwayRoutesDBContext _subwayRoutesDBContext;

        public CityRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }
        public async Task<CityEntity> Add(CityEntity entity)
        {
            await _subwayRoutesDBContext.Citys.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return entity;
        }
        public async Task<CityEntity> Update(CityEntity entity)
        {
            var upDateEntity = _subwayRoutesDBContext.Citys.Update(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return upDateEntity.Entity;
        }
        public async Task<CityEntity> Get(int idEntity)
        {

            var result = await _subwayRoutesDBContext.Citys.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }
        public async Task<CityEntity> Update(int idEntity, CityEntity upDateEntity)
        {
            var entity = await Get(idEntity);
            entity.Name = upDateEntity.Name;

            var result = _subwayRoutesDBContext.Update(entity);

            return entity;
        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<CityEntity>> GetAll()
        {
            throw new NotImplementedException();
        }



        public Task<CityEntity> Update(int idEntity)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.Citys.SingleAsync(x => x.Id == id);
            _subwayRoutesDBContext.Citys.Remove(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();
        }
    }
}
