﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private ISubwayRoutesDBContext _subwayRoutesDBContext;

        public UserRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }
        public async Task<UserEntity> Add(UserEntity entity)
        {
            await _subwayRoutesDBContext.Users.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return entity;
        }
        public async Task<UserEntity> Update(UserEntity entity)
        {
            var upDateEntity = _subwayRoutesDBContext.Users.Update(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return upDateEntity.Entity;
        }
        public async Task<UserEntity>Get(int idEntity)
        {
            
            var result = await _subwayRoutesDBContext.Users.FirstOrDefaultAsync(x =>x.Id==idEntity);
            return result;
        }
        public async Task<UserEntity> Update(int  idEntity, UserEntity upDateEntity)
        {
            var entity = await Get(idEntity);
            entity.Name = upDateEntity.Name;

            var result =_subwayRoutesDBContext.Update(entity);

            return entity;
        }
       
        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<UserEntity>> GetAll()
        {
            throw new NotImplementedException();
        }

        

        public Task<UserEntity> Update(int idEntity)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.Users.SingleAsync(x => x.Id == id);
            _subwayRoutesDBContext.Users.Remove(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();
        }
    }
}
