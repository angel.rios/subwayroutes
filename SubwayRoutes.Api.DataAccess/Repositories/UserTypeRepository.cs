﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class UserTypeRepository : IUserTypeRepository
    {
        private ISubwayRoutesDBContext _subwayRoutesDBContext;

        public UserTypeRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }
        public async Task<UserTypeEntity> Add(UserTypeEntity entity)
        {
            await _subwayRoutesDBContext.UsersType.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return entity;
        }
        public async Task<UserTypeEntity> Update(UserTypeEntity entity)
        {
            var upDateEntity = _subwayRoutesDBContext.UsersType.Update(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();

            return upDateEntity.Entity;
        }
        public async Task<UserTypeEntity> Get(int idEntity)
        {

            var result = await _subwayRoutesDBContext.UsersType.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }
        public async Task<UserTypeEntity> Update(int idEntity, UserTypeEntity upDateEntity)
        {
            var entity = await Get(idEntity);
            entity.TypeName = upDateEntity.TypeName;

            var result = _subwayRoutesDBContext.Update(entity);

            return entity;
        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<UserEntity>> GetAll()
        {
            throw new NotImplementedException();
        }



        public Task<UserTypeEntity> Update(int idEntity)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.UsersType.SingleAsync(x => x.Id == id);
            _subwayRoutesDBContext.UsersType.Remove(entity);

            await _subwayRoutesDBContext.SaveChangeAsync();
        }

        Task<IEnumerable<UserTypeEntity>> IRepository<UserTypeEntity>.GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
