﻿using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.Services.Services
{
    public class UserServices
    {

        private readonly IUserRepository _userRepository;
        public UserServices(IUserRepository userReposiroty)
        {
            _userRepository = userReposiroty;

        }

        public async Task<string> GetUserPass(int id)
        {
            var entidad = await _userRepository.Get(id);
            return entidad.Password;
        }

        }

    }

