﻿using Microsoft.Extensions.DependencyInjection;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using SubwayRoutes.Api.DataAccess.Repositories;
using SubwayRoutes.Api.Services;
using SubwayRoutes.Api.Services.Contracts.Services;

namespace SubwayRoutes.Api.Transversal.Register
{
    public static class IoCRegister
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {

            AddRegisterServices(services);
            AddRegisterRepositories(services);
            //AddRegisterOthers(services);

            return services;

        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {
            NewMethod(services);

            return services;
        }

        private static void NewMethod(IServiceCollection services)
        {
        }

        private static IServiceCollection AddRegisterRepositories(IServiceCollection services)
        {

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserTypeRepository, UserTypeRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IRouteRepository, RouteRepository>();
            services.AddTransient<IRegisterRepository, RegisterRepository>();

            return services;
        }

        //private static IServiceCollection AddRegisterOthers(IServiceCollection services)
        //{

        //    services.AddTransient<IAppConfig, AppConfig>();
        //    services.AddTransient<IApiCaller, ApiCaller>();


        //    return services;
        //}

    }
}